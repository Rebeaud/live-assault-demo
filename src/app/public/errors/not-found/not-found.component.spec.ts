import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFoundComponent } from './not-found.component';
import { Component } from '@angular/core';
import { Settings } from '../../../app.settings.model';
import { AppSettings } from '../../../app.settings';
import { RouterTestingModule } from '@angular/router/testing';

describe('NotFoundComponent', () => {
  let component: NotFoundComponent;
  let fixture: ComponentFixture<NotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        NotFoundComponent,
        MockMatIcon,
        MockMatCard,
        MockMatCardContent,
        MockMatSidenavCont,
        MockMatFormField
      ],
      providers: [Settings, AppSettings]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: 'mat-icon',
  template: ''
})
class MockMatIcon {}

@Component({
  selector: 'mat-card',
  template: ''
})
class MockMatCard {}

@Component({
  selector: 'mat-card-content',
  template: ''
})
class MockMatCardContent {}

@Component({
  selector: 'mat-sidenav-container',
  template: ''
})
class MockMatSidenavCont {}

@Component({
  selector: 'mat-form-field',
  template: ''
})
class MockMatFormField {}
