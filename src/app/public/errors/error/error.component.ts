import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Settings } from '../../../app.settings.model';
import { AppSettings } from '../../../app.settings';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html'
})
export class ErrorComponent implements AfterViewInit {
  public settings: Settings;

  constructor(public appSettings: AppSettings, public router: Router) {
    this.settings = this.appSettings.settings;
  }

  goHome(): void {
    this.router.navigate(['/']);
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}
