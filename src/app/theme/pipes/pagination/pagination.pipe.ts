import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {
  transform(data, args?): Array<any> {
    if (!args) {
      args = {
        pageIndex: 0,
        pageSize: 6,
        length: data.length
      };
    }
    return this.paginate(data, args.pageSize, args.pageIndex);
  }

  paginate(array, pageSize, pageNumber) {
    return array.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize);
  }
}
